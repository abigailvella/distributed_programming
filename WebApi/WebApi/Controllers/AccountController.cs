﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using WebApi.Models;
using WebApi.Providers;

namespace WebApi.Controllers
{
    public class AccountController : Controller
    {
        private string baseUrl = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority;

        // GET: Account
        public ActionResult Register()
        {
            return View();
        }

        //POST: Account
        [HttpPost]
        public ActionResult Register(RegisterBindingModel user)
        {
            if(ModelState.IsValid)
            {
                HttpClient httpClient = new HttpClient();

                httpClient.BaseAddress = new Uri(baseUrl + "/API/Account/Register");

                var response = httpClient.PostAsJsonAsync<RegisterBindingModel>("Register", user);

                response.Wait();

                var result = response.Result;

                if(result.IsSuccessStatusCode) //200
                {
                    ViewBag.Message = "Register Successfull";
                }
                else
                {
                    ViewBag.Error = result.Content.ReadAsStringAsync().Result;
                }
            }

            return View();
        }

        //GET: Login
        public ActionResult Login()
        {
            return View();
        }

        //GET: Home
        public ActionResult Home()
        {
            //var NumberofFeeds = 3;
            //string PageId = "YOUR_FACEBOOK_PAGE_NAME_HERE";
            //string AccessToken = "PLACE_YOUR_ACCESS_TOKEN_HERE";
            //FBPostsModel posts;
            //string FeedRequestUrl = string.Concat("https://graph.facebook.com/" + PageId + "/posts?limit=", NumberofFeeds, "&access_token=", AccessToken);
            //HttpWebRequest feedRequest = (HttpWebRequest)WebRequest.Create(FeedRequestUrl);
            //feedRequest.Method = "GET";
            //feedRequest.Accept = "application/json";
            //feedRequest.ContentType = "application/json; charset=utf-8";
            //WebResponse feedResponse = (HttpWebResponse)feedRequest.GetResponse();
            //using (feedResponse)
            //{
            //    using (var reader = new StreamReader(feedResponse.GetResponseStream()))
            //    {
            //        posts = JsonConvert.DeserializeObject<FBPostsModel>(reader.ReadToEnd());
            //    }
            //}
            //return View(posts);

            return View();
        }

        public ActionResult FacebookFeed()
        {
            return View();
        }
    }
}